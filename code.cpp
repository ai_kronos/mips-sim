#include<cstdio>
#include<iostream>
#include<cstdlib>
#include<cstring>
#include<string>
#include<map>
#include<vector>
#include<queue>
#include<fstream>
using namespace std;

const int command_len = 10;
const int address_name_len = 50;

vector<char> _memory;
map<string, int> _label;

ifstream _file;

int hs_p;

int de(20);

template<class T>
void static_append(T &a)
{
    int n(sizeof(T));
    char *p(reinterpret_cast<char*> (&a));
    for(int i = 0; i < n; ++i, ++p)
        _memory.push_back(*p);
}

template<class T>
void str_append(T a, char *w)
{
    T *p(reinterpret_cast<T*>(w));
    *p = a;
}

int fetch_int(const char *b)
{
    const int *p(reinterpret_cast<const int*>(b));
    return *p;
}

bool _str_cmp(const char *a, const char *b)
{
    for(;*a == *b && (*a) != '\0'; ++a, ++b);
    return (*a) == '\0' && (*b) == '\0';
}

struct _command
{
	int com;
    char add[3][address_name_len];
    _command()
    {
		com = -2;
        for(int i = 0; i < 3; ++i)
            memset(add[i], 0, sizeof(add[i]));
    }
};

struct _command_short
{
	int com;
    char add[4][5], num;
    _command_short(): num(0)
    {
		com = -2;
        for(int i = 0; i < 4; ++i)
            memset(add[i], 0, sizeof(add[i]));
    }
};

constexpr bool is_number(char c)
{
    return c >= '0' && c <= '9';
}

bool command_group1(int v)
{
	if(v == 1 || v == 2)
		return 1;
	if(v >= 7 && v <= 12)
		return 1;
	if(v >= 14 && v <= 19)
		return 1;
	return 0;
}

int get_command_index(const char *s)
{
	if(s[0] == '.')
		return -1;
	if(s[0] == '@')
		return 0;
	if (_str_cmp(s, "add"))
		return 1;
	if (_str_cmp(s, "addu"))
		return 1;
	if (_str_cmp(s, "addiu"))
		return 1;
	if (_str_cmp(s, "subu"))
		return 2;
	if (_str_cmp(s, "sub"))
		return 2;
	if (_str_cmp(s, "mul"))
		return 3;
	if (_str_cmp(s, "mulu"))
		return 4;
	if (_str_cmp(s, "div"))
		return 5;
	if (_str_cmp(s, "divu"))
		return 6;
	if (_str_cmp(s, "xor"))
		return 7;
	if (_str_cmp(s, "xoru"))
		return 8;
	if (_str_cmp(s, "neg"))
		return 9;
	if (_str_cmp(s, "negu"))
		return 10;
	if (_str_cmp(s, "rem"))
		return 11;
	if (_str_cmp(s, "remu"))
		return 12;
	if (_str_cmp(s, "li"))
		return 13;
	if (_str_cmp(s, "seq"))
		return 14;
	if (_str_cmp(s, "sge"))
		return 15;
	if (_str_cmp(s, "sgt"))
		return 16;
	if (_str_cmp(s, "sle"))
		return 17;
	if (_str_cmp(s, "slt"))
		return 18;
	if (_str_cmp(s, "sne"))
		return 19;
	if (_str_cmp(s, "b"))
		return 20;
	if (_str_cmp(s, "beq"))
		return 21;
	if (_str_cmp(s, "bne"))
		return 22;
	if (_str_cmp(s, "bge"))
		return 23;
	if (_str_cmp(s, "ble"))
		return 24;
	if (_str_cmp(s, "bgt"))
		return 25;
	if (_str_cmp(s, "blt"))
		return 26;
	if (_str_cmp(s, "beqz"))
		return 27;
	if (_str_cmp(s, "bnez"))
		return 28;
	if (_str_cmp(s, "blez"))
		return 29;
	if (_str_cmp(s, "bgez"))
		return 30;
	if (_str_cmp(s, "bgtz"))
		return 31;
	if (_str_cmp(s, "bltz"))
		return 32;
	if (_str_cmp(s, "j"))
		return 33;
	if (_str_cmp(s, "jr"))
		return 34;
	if (_str_cmp(s, "jal"))
		return 35;
	if (_str_cmp(s, "jalr"))
		return 36;
	if (_str_cmp(s, "la"))
		return 37;
	if (_str_cmp(s, "lb"))
		return 38;
	if (_str_cmp(s, "lh"))
		return 39;
	if (_str_cmp(s, "lw"))
		return 40;
	if (_str_cmp(s, "sb"))
		return 41;
	if (_str_cmp(s, "sh"))
		return 42;
	if (_str_cmp(s, "sw"))
		return 43;
	if (_str_cmp(s, "move"))
		return 44;
	if (_str_cmp(s, "mfhi"))
		return 45;
	if (_str_cmp(s, "mflo"))
		return 46;
	if (_str_cmp(s, "nop"))
		return 47;
	if (_str_cmp(s, "syscall"))
		return 48;
}

bool is_mu(int v)
{
	return v == 3 || v == 4;
}

bool is_di(int v)
{
	return v == 5 || v == 6;
}

bool is_b_pre(int v)
{
	return v >= 20 && v <= 32;
}

bool is_b1(int v)
{
	return v >= 21 && v <= 26;
}

bool is_b2(int v)
{
	return v >= 27 && v <= 32;
}

bool is_j_pre(int v)
{
	return v >= 33 && v <= 36;
}

bool is_jal(int v)
{
	return v == 35 || v == 36;
}

bool is_sy(int v)
{
	return v == 48;
}

bool is_store(int v)
{
	return v >= 41 && v <= 43;
}

bool is_lo_hi(int v)
{
	return v >= 3 && v <= 6;
}

bool is_mf(int v)
{
	return v == 45 || v == 46;
}

void load_p()
{
    string s, s2;
    int status = -1;
    while(_file >> s)
    {
		if(s[0] == '#')
		{
			char c;
			_file.get(c);
			while(c != '\n')
				_file.get(c);
			_file >> s;
		}
        if(s[s.length() - 1] == ':')
        {
            s.pop_back();
            _label[s] = _memory.size();
        }
        else if(s == ".data")
            status = 0;
        else if(s == ".text")
            status = 1;
        else if(s == ".align")
        {
            int n, t;
            _file >> n;
            n = (1 << n);
            t = (n - _memory.size() % n) % n;
            for(int i = 0; i < t; ++i)
                _memory.push_back(0);
        }
        else if(status == 0)
        {
            if (s[1] == 'a')
            {
                int mcnt(0);
                unsigned int i;
                do
                {
                    i = 0;
                    _file >> s2;
                    if (s2[0] == '\"')
                    {
                        i = 1;
                        ++mcnt;
                        if (mcnt == 2)
                            _memory.push_back(' ');
                    }
                    else
                        _memory.push_back(' ');
                    for (; i < s2.length() && s2[i] != '\"'; ++i)
                    {
                        if (s2[i] == '\\')
                        {
                            ++i;
                            switch (s2[i])
                            {
                            case 'a':
                                s2[i] = 7;
                                break;
                            case 'b':
                                s2[i] = 8;
                                break;
                            case 'f':
                                s2[i] = 12;
                                break;
                            case 'n':
                                s2[i] = 10;
                                break;
                            case 'r':
                                s2[i] = 13;
                                break;
                            case 't':
                                s2[i] = 9;
                                break;
                            case 'v':
                                s2[i] = 11;
                                break;
                            case '\\':
                                s2[i] = 92;
                                break;
                            case '\'':
                                s2[i] = 39;
                                break;
                            case '\"':
                                s2[i] = 34;
                                break;
                            case '\?':
                                s2[i] = 63;
                                break;
                            case '\0':
                                s2[i] = 0;
                                break;
                            }
                        }
                        _memory.push_back(s2[i]);
                    }
                } while (s2[i] != '\"' && mcnt != 2);
                if (s[s.length() - 1] == 'z')
                    _memory.push_back('\0');
            }
            else if (s == ".space")
            {
                int n;
                _file >> n;
                for (int i = 0; i < n; ++i)
                    _memory.push_back(0);
            }
            else
            {
                int n(1);
                if (s == ".half")
                    n = 2;
                if (s == ".word")
                    n = 4;
                int t;
                do
                {
                    _file >> t;
                    char *p(reinterpret_cast<char *>(&t));
                    for (int i = 0; i < n; ++i, ++p)
                        _memory.push_back(*p);
                } while (_file.get() == ',');
            }
        }
        else if (status == 1)
        {
            _command tmp;
			tmp.com = get_command_index(s.c_str());
            if (s != "nop" && s != "syscall")
            {
                int i = 0;
                for (_file >> s; s[s.length() - 1] == ','; ++i, _file >> s)
                {
                    s.pop_back();
                    strcpy(tmp.add[i], s.c_str());
                }
                strcpy(tmp.add[i], s.c_str());
            }
            static_append(tmp);
        }
    }
}

class _register
{
  private:
    char v[4];
    int flag;

  public:
    _register() : flag(0)
    {
        memset(v, 0, sizeof(v));
    }
    _register &operator=(const int &o)
    {
        int *p(reinterpret_cast<int *>(v));
        *p = o;
		--flag;
        return *this;
    }
    _register &operator=(const char *s)
    {
        strcpy(v, s);
		--flag;
        return *this;
    }
    _register &operator=(string s)
    {
        strcpy(v, s.c_str());
		--flag;
        return *this;
    }
	bool is_lock()
	{
		if(flag)
			return 1;
		return 0;
	}
    void lock()
    {
		++flag;
    }
	void unlock()
	{
		--flag;
	}
    int get_int_at_risk()
    {
        const int *p(reinterpret_cast<int *>(v));
        int tmp(*p);
        return tmp;
    }
    int get_int()
    {
        const int *p(reinterpret_cast<int *>(v));
        int tmp(*p);
        return tmp;
    }
} _re[35];

int PC;

int stage_zero(_command_short &tmp)
{
	static int cnt(0);
	if(de == 0)printf("0: %d %d\n", ++cnt, (int)PC);
    if (PC > 0)
    {
        str_append((int)PC, tmp.add[3]);
        char *t(&_memory[PC]);
		_command *p(reinterpret_cast<_command *>(t));
		if(de == 0)printf("[%d] %d %s %s %s\n",(int)PC, p -> com, p -> add[0], p -> add[1], p -> add[2]);
        if(p -> com == -1)
        return 2;
        if (!(is_b_pre(p -> com) || is_j_pre(p -> com) || is_sy(p -> com)))
            return 0;
        /*
        pass
		*/
		tmp.num = 0;
		return tmp.num;
	}
	else
    {
        tmp.com = 0;
        return -1;
	}
}

constexpr int get_reg_id(const char *s)
{
    if (is_number(s[1]))
    {
        int val(0);
        for (int i = 1; is_number(s[i]); ++i)
        {
            val *= 10;
            val += s[i] - '0';
        }
        return val;
    }
    if (s[1] == 'z')
        return 0;
    if (s[1] == 'a')
    {
        if (s[2] == 't')
            return 1;
        return 4 + s[2] - '0';
    }
    if (s[1] == 'v')
        return 2 + s[2] - '0';
    if (s[1] == 't')
    {
        if (s[2] < '8')
            return 8 + s[2] - '0';
        else
            return 24 + s[2] - '8';
    }
    if (s[1] == 's')
    {
        if (s[2] == 'p')
            return 29;
        return 16 + s[2] - '0';
    }
    if (s[1] == 'g')
        return 28;
    if (s[1] == 'f')
        return 30;
    if (s[1] == 'r')
        return 31;
    if (s[1] == 'h')
        return 32;
    if (s[1] == 'l')
        return 33;
    if (s[1] == 'p')
        return 34;
}

int get_command_type(int v)
{
	return is_b_pre(v) || is_j_pre(v) || is_store(v);
}

int anay(char *s, char *t, char *t2 = NULL)
{
	if(s[49] == 1)
	{
		str_append(fetch_int(s), t);
		return 0;
	}
    int re_id;
	if(s[49] == 2)
	{
		re_id = fetch_int(s);
		if(_re[re_id].is_lock())
			return 1;
        str_append(_re[re_id].get_int(), t);
		return 0;
	}
	if(s[49] == 3)
	{
		str_append(fetch_int(s), t);
		re_id = fetch_int(s + 4);
		if(_re[re_id].is_lock())
			return 1;
        str_append(_re[re_id].get_int(), t2);
        return 0;
	}
    if (s[0] == '$')
    {
        re_id = get_reg_id(s);
		s[49] = 2;
		str_append(re_id, s);
		if(_re[re_id].is_lock())
			return 1;
        str_append(_re[re_id].get_int(), t);
        return 0;
    }
    if (is_number(s[0]) || s[0] == '-')
    {
        int offset = 0;
        int flag = 1, i = 0;
        if (s[0] == '-')
        {
            flag = -1;
            i = 1;
        }
        while (is_number(s[i]))
        {
            offset *= 10;
            offset += s[i] - '0';
            ++i;
        }
        offset *= flag;
        str_append(offset, t);
		s[49] = 1;
        if (s[i] != '(')
		{
			str_append(offset, s);
			return 0;
		}
		s[49] = 3;
        re_id = get_reg_id(s + i + 1);
		str_append(offset, s);
		str_append(re_id, s + 4);
		if(_re[re_id].is_lock())
			return 1;
        str_append(_re[re_id].get_int(), t2);
        return 0;
    }
	re_id = _label[s];
	str_append(re_id, t);
	str_append(re_id, s);
	s[49] = 1;
	return 0;
}

int label_ad, lock_id, lock_id_2;
int Flag;

int stage_one(_command_short &tmp)
{
	static int cnt(0);
	if (de == 1)
		printf("1: %d\n", ++cnt);
	if (tmp.com == 0)
		return -1;
	int ad(fetch_int(tmp.add[3]));
	lock_id = lock_id_2 = -1;
	_command *p(reinterpret_cast<_command *>(&_memory[ad]));
	tmp.com = p->com;
	if (de == 1)
		printf("[]%d %s %s %s\n", tmp.com, p->add[0], p->add[1], p->add[2]);
	if (fetch_int(p->add[0]) != 0)
	{
		int re_id;
		if (get_command_type(tmp.com) || (fetch_int(p->add[2]) == 0 && is_lo_hi(tmp.com)))
		{
			if (p->add[0][0] != '$')
			{
				if (p->add[0][49] == 0)
				{
					re_id = _label[p->add[0]];
					str_append(re_id, p->add[0] + 1);
					p->add[0][49] = 1;
				}
				else
					re_id = fetch_int(p->add[0] + 1);
			}
			else
			{
				if(_re[get_reg_id(p->add[0])].is_lock())
					return -2;
				re_id = _re[get_reg_id(p->add[0])].get_int();
			}
			if (fetch_int(p->add[2]) == 0 && is_lo_hi(tmp.com))
			{
				lock_id = get_reg_id("$lo");
				lock_id_2 = get_reg_id("$hi");
			}
			if (is_jal(tmp.com))
			{
				str_append(ad, tmp.add[1]);
				lock_id = 31;
			}
		}
		else
		{
			re_id = get_reg_id(p->add[0]);
			lock_id = re_id;
		}
		str_append(re_id, tmp.add[0]);
	}
	if (fetch_int(p->add[1]) != 0 || p -> add[1][49])
		if(anay(p->add[1], tmp.add[1], tmp.add[2]))
			return -2;
	if (fetch_int(p->add[2]) != 0 || p -> add[2][49])
	{
		if(anay(p->add[2], tmp.add[2]))
			return -2;
		if (is_lo_hi(tmp.com))
			tmp.num = 1;
	}
	if (is_mf(tmp.com))
		if (tmp.com == 45)
		{
			if(_re[32].is_lock())
				return -2;
			str_append(_re[32].get_int(), tmp.add[1]);
		}
		else
		{
			if(_re[33].is_lock())
				return -2;
			str_append(_re[33].get_int(), tmp.add[1]);
		}
	if (is_sy(tmp.com))
	{
		if(_re[get_reg_id("$v0")].is_lock())
			return -2;
		int type(_re[get_reg_id("$v0")].get_int());
		str_append(type, tmp.add[0]);
		switch (type)
		{
		case 1:
		case 4:
			if (_re[get_reg_id("$a0")].is_lock())
				return -2;
			str_append(_re[get_reg_id("$a0")].get_int(), tmp.add[1]);
			break;
		case 5:
			lock_id = get_reg_id("$v0");
			break;
		case 8:
			if (_re[get_reg_id("$a0")].is_lock())
				return -2;
			if (_re[get_reg_id("$a1")].is_lock())
				return -2;
			str_append(_re[get_reg_id("$a0")].get_int(), tmp.add[1]);
			str_append(_re[get_reg_id("$a1")].get_int(), tmp.add[2]);
			break;
		case 9:
			if (_re[get_reg_id("$a0")].is_lock())
				return -2;
			str_append(_re[get_reg_id("$a0")].get_int(), tmp.add[1]);
			lock_id = get_reg_id("$v0");
			break;
		case 17:
			if (_re[get_reg_id("$a0")].is_lock())
				return -2;
			str_append(_re[get_reg_id("$a0")].get_int(), tmp.add[1]);
			break;
		}
	}
	if (de == 1)
		printf("[res]%d %d %d %d\n", tmp.com, fetch_int(tmp.add[0]), fetch_int(tmp.add[1]), fetch_int(tmp.add[2]));
	if (tmp.num)
	{
		if (is_b1(tmp.com))
		{
			label_ad = fetch_int(tmp.add[2]);
			return 1;
		}

		if (is_b2(tmp.com))
		{
			label_ad = fetch_int(tmp.add[1]);
			return 1;
		}

		if (tmp.com == 20 || tmp.com == 33)
		{
			label_ad = fetch_int(tmp.add[0]);
			return 1;
		}
	}
	if (!Flag && lock_id >= 0)
		_re[lock_id].lock();
	if (!Flag && lock_id_2 >= 0)
		_re[lock_id_2].lock();
	return 0;
}

int label_ad_2;

int stage_two(_command_short &ob)
{
	if (ob.com == 0)
		return -1;
	static int cnt(0);
	if (de == 2)
		printf("2: %d\n", ++cnt);
	if (de == 2)
		printf("[]%d %d %d %d\n", ob.com, fetch_int(ob.add[0]), fetch_int(ob.add[1]), fetch_int(ob.add[2]));
	if (ob.com == 1)
		str_append(fetch_int(ob.add[1]) + fetch_int(ob.add[2]), ob.add[1]);
	if (ob.com == 2)
		str_append(fetch_int(ob.add[1]) - fetch_int(ob.add[2]), ob.add[1]);
	if (is_mu(ob.com))
		if (ob.com == 4)
			if (ob.num)
			{
				unsigned int a(fetch_int(ob.add[1])), b(fetch_int(ob.add[2]));
				str_append(a * b, ob.add[1]);
			}
			else
			{
				unsigned long long a(fetch_int(ob.add[0])), b(fetch_int(ob.add[1]));
				str_append(a * b, ob.add[0]);
			}
		else if (ob.num)
		{
			int a(fetch_int(ob.add[1])), b(fetch_int(ob.add[2]));
			str_append(a * b, ob.add[1]);
		}
		else
		{
			long long a(fetch_int(ob.add[0])), b(fetch_int(ob.add[1]));
			str_append(a * b, ob.add[0]);
		}
	if (is_di(ob.com))
		if (ob.com == 6)
			if (ob.num)
			{
				unsigned int a(fetch_int(ob.add[1])), b(fetch_int(ob.add[2]));
				str_append(a / b, ob.add[1]);
			}
			else
			{
				unsigned int a(fetch_int(ob.add[0])), b(fetch_int(ob.add[1]));
				str_append(a / b, ob.add[0]);
				str_append(a % b, ob.add[1]);
			}
		else if (ob.num)
		{
			int a(fetch_int(ob.add[1])), b(fetch_int(ob.add[2]));
			str_append(a / b, ob.add[1]);
		}
		else
		{
			unsigned int a(fetch_int(ob.add[0])), b(fetch_int(ob.add[1]));
			str_append(a / b, ob.add[0]);
			str_append(a % b, ob.add[1]);
		}
	if (ob.com == 7 || ob.com == 8)
		str_append((fetch_int(ob.add[1]) ^ fetch_int(ob.add[2])), ob.add[1]);
	if (ob.com == 9 || ob.com == 10)
		if (ob.com == 10)
		{
			unsigned int a(fetch_int(ob.add[1]));
			str_append((~a), ob.add[1]);
		}
		else
			str_append(-fetch_int(ob.add[1]), ob.add[1]);
	if (ob.com == 11 || ob.com == 12)
		if (ob.com == 12)
		{
			unsigned int a(fetch_int(ob.add[1])), b(fetch_int(ob.add[2]));
			str_append(a % b, ob.add[1]);
		}
		else
			str_append(fetch_int(ob.add[1]) % fetch_int(ob.add[2]), ob.add[1]);
	if (ob.com == 14)
		str_append((int)(fetch_int(ob.add[1]) == fetch_int(ob.add[2])), ob.add[1]);
	if (ob.com == 15)
		str_append((int)(fetch_int(ob.add[1]) >= fetch_int(ob.add[2])), ob.add[1]);
	if (ob.com == 16)
		str_append((int)(fetch_int(ob.add[1]) > fetch_int(ob.add[2])), ob.add[1]);
	if (ob.com == 17)
		str_append((int)(fetch_int(ob.add[1]) <= fetch_int(ob.add[2])), ob.add[1]);
	if (ob.com == 18)
		str_append((int)(fetch_int(ob.add[1]) < fetch_int(ob.add[2])), ob.add[1]);
	if (ob.com == 19)
		str_append((int)(fetch_int(ob.add[1]) != fetch_int(ob.add[2])), ob.add[1]);
	if (ob.com == 21)
		str_append((int)(fetch_int(ob.add[0]) == fetch_int(ob.add[1])), ob.add[0]);
	if (ob.com == 22)
		str_append((int)(fetch_int(ob.add[0]) != fetch_int(ob.add[1])), ob.add[0]);
	if (ob.com == 23)
		str_append((int)(fetch_int(ob.add[0]) >= fetch_int(ob.add[1])), ob.add[0]);
	if (ob.com == 24)
		str_append((int)(fetch_int(ob.add[0]) <= fetch_int(ob.add[1])), ob.add[0]);
	if (ob.com == 25)
		str_append((int)(fetch_int(ob.add[0]) > fetch_int(ob.add[1])), ob.add[0]);
	if (ob.com == 26)
		str_append((int)(fetch_int(ob.add[0]) < fetch_int(ob.add[1])), ob.add[0]);
	if (ob.com == 27)
		str_append((int)(fetch_int(ob.add[0]) == 0), ob.add[0]);
	if (ob.com == 28)
		str_append((int)(fetch_int(ob.add[0]) != 0), ob.add[0]);
	if (ob.com == 29)
		str_append((int)(fetch_int(ob.add[0]) <= 0), ob.add[0]);
	if (ob.com == 30)
		str_append((int)(fetch_int(ob.add[0]) >= 0), ob.add[0]);
	if (ob.com == 31)
		str_append((int)(fetch_int(ob.add[0]) > 0), ob.add[0]);
	if (ob.com == 32)
		str_append((int)(fetch_int(ob.add[0]) < 0), ob.add[0]);
	if (is_jal(ob.com))
		str_append(fetch_int(ob.add[1]) + sizeof(_command), ob.add[1]);
	if (ob.com >= 37 && ob.com <= 43)
		str_append(fetch_int(ob.add[1]) + fetch_int(ob.add[2]), ob.add[1]);
	if (de == 2)
		printf("[]%d %d %d %d\n", ob.com, fetch_int(ob.add[0]), fetch_int(ob.add[1]), fetch_int(ob.add[2]));
	if(Flag)
		return 0;

	if (is_b1(ob.com))
	{
		if (fetch_int(ob.add[0]) != ob.num)
		{
			if (ob.num)
				label_ad_2 = fetch_int(ob.add[3]) + sizeof(_command);
			else
				label_ad_2 = fetch_int(ob.add[2]);
			return 1;
		}
	}

	if (is_b2(ob.com))
	{
		if (fetch_int(ob.add[0]) != ob.num)
		{
			if (ob.num)
				label_ad_2 = fetch_int(ob.add[3]) + sizeof(_command);
			else
				label_ad_2 = fetch_int(ob.add[1]);
			return 1;
		}
	}

	if (ob.com == 20 || ob.com == 33 || ob.com == 34)
	{
		if (!ob.num)
		{
			label_ad_2 = fetch_int(ob.add[0]);
			return 1;
		}
	}

	if (is_jal(ob.com))
	{
		_re[31] = fetch_int(ob.add[1]);
		if (!ob.num)
		{
			label_ad_2 = fetch_int(ob.add[0]);
			return 1;
		}
	}

	if (is_sy(ob.com))
	{
		int type(fetch_int(ob.add[0]));
		switch (type)
		{
		case 10:
		case 17:
			label_ad_2 = -1;
			return 1;
			break;
		}
	}
	return 0;
}

int stage_three(_command_short &ob)
{
	if (ob.com == 0)
		return -1;
	static int cnt = 0;
	if (de == 3)
		printf("3: %d\n[]%d %d %d %d\n", ++cnt, ob.com, fetch_int(ob.add[0]), fetch_int(ob.add[1]), fetch_int(ob.add[2]));
	if (ob.com == 0)
		return -1;
	if (ob.com >= 38 && ob.com <= 40)
	{
		int n = 1;
		if (ob.com == 39)
			n = 2;
		if (ob.com == 40)
			n = 4;
		int ad(fetch_int(ob.add[1]));
		str_append(0, ob.add[1]);
		for (int i = 0; i < n; ++i, ++ad)
			ob.add[1][i] = _memory[ad];
	}
	if (is_store(ob.com))
	{
		int n = 1;
		if (ob.com == 42)
			n = 2;
		if (ob.com == 43)
			n = 4;
		int ad(fetch_int(ob.add[1]));
		for (int i = 0; i < n; ++i, ++ad)
			_memory[ad] = ob.add[0][i];
	}
	if (is_sy(ob.com))
	{
		int type(fetch_int(ob.add[0]));
		switch (type)
		{
		case 1:
		{
			printf("%d", fetch_int(ob.add[1]));
			break;
		}
		case 4:
		{
			int ad(fetch_int(ob.add[1]));
			while (_memory[ad] != '\0')
				printf("%c", _memory[ad++]);
			break;
		}
		case 5:
		{
			int tmp;
			cin >> tmp;
			str_append(tmp, ob.add[1]);
			break;
		}
		case 8:
		{
			int ad(fetch_int(ob.add[1])), len(fetch_int(ob.add[2]));
			--len;
			string tmp;
			cin >> tmp;
			int l(tmp.length());
			l = min(l, len);
			for (int i = 0; i < l; ++i, ++ad)
				_memory[ad] = tmp[i];
			_memory[ad] = '\0';
			break;
		}
		case 9:
		{
			int len(fetch_int(ob.add[1]));
			str_append(hs_p, ob.add[1]);
			hs_p += len;
			break;
		}
		}
	}
	if (de == 3)
		printf("[res]%d %d %d %d\n", ob.com, fetch_int(ob.add[0]), fetch_int(ob.add[1]), fetch_int(ob.add[2]));
	return 0;
}

int stage_four(_command_short &ob)
{
	static int cnt = 0;
	if (de == 4)
		printf("4: %d\n[]%d %d %d %d\n", ++cnt, ob.com, fetch_int(ob.add[0]), fetch_int(ob.add[1]), fetch_int(ob.add[2]));
	if (ob.com == 0)
		return -1;
	if (command_group1(ob.com))
		_re[fetch_int(ob.add[0])] = fetch_int(ob.add[1]);

	if (ob.com >= 3 && ob.com <= 6)
		if (ob.num)
			_re[fetch_int(ob.add[0])] = fetch_int(ob.add[1]);
		else
		{
			_re[get_reg_id("$lo")] = fetch_int(ob.add[0]);
			_re[get_reg_id("$hi")] = fetch_int(ob.add[1]);
		}

	if (ob.com == 13 || ob.com >= 37 && ob.com <= 40)
		_re[fetch_int(ob.add[0])] = fetch_int(ob.add[1]);
	if (ob.com >= 44 && ob.com <= 46)
		_re[fetch_int(ob.add[0])] = fetch_int(ob.add[1]);

	if (is_sy(ob.com))
	{
		int type(fetch_int(ob.add[0]));
		switch (type)
		{
		case 5:
			_re[get_reg_id("$v0")] = fetch_int(ob.add[1]);
			break;
		case 9:
			_re[get_reg_id("$v0")] = fetch_int(ob.add[1]);
			break;
		}
	}
	return 0;
}

bool Sig[5], is_run[5], sto[5];
_command_short buffer[5];
int Cnt(31), F[5];

void work(int (*fun)(_command_short &), int index)
{
	if (is_run[index])
	{
		if (!Sig[index])
			return;
		F[index] = fun(buffer[index]);
		Sig[index] = 0;
		if (F[index] == -1)
		{
			is_run[index] = 0;
			Cnt ^= (1 << index);
		}
		if (F[index] >= -1)
			sto[index] = 1;
	}
}

inline void _Notify_all()
{
	for (int i = 0; i < 5; ++i)
		F[i] = -3;
	work(stage_four, 4);
	work(stage_zero, 0);
	work(stage_three, 3);
	work(stage_two, 2);
	work(stage_one, 1);
}

void run()
{
	while (Cnt)
	{
		//		printf("start %d %d:\n", (int)PC, (int)Cnt);
		if (sto[3])
		{
			buffer[4] = buffer[3];
			sto[3] = 0;
			Sig[4] = 1;
		}
		if (sto[2])
		{
			buffer[3] = buffer[2];
			sto[2] = 0;
			Sig[3] = 1;
		}
		if (sto[1])
		{
			buffer[2] = buffer[1];
			sto[1] = 0;
			Sig[2] = 1;
		}
		int f1(F[1]);
		if (f1 == -2)
			Sig[1] = 1;
		else if (sto[0])
		{
			buffer[1] = buffer[0];
			sto[0] = 0;
			Sig[1] = 1;
		}
		if (f1 != -2 && F[0] < 1)
		{
			buffer[0] = _command_short();
			Sig[0] = 1;
		}
		/*
		printf("beg:\n");
		for(int i = 0; i < 5; ++i)
			printf("%d ", (int)Sig[i]);
		printf("%d \n", (int)Cnt);
		*/
		_Notify_all();
		//		for(int i = 0; i < 300000000; ++i);
		/*
		printf("End\n\n");
		printf("      ");
		for(int i = 0; i < 5; ++i)
			printf("%d ", (int)F[i]);
		printf("\n");
		*/
		if (F[0] == 0)
			PC += sizeof(_command);
		if (F[2] == 1)
		{
			if (F[1] == 0)
			{
				if (lock_id >= 0)
					_re[lock_id].unlock();
				if (lock_id_2 >= 0)
					_re[lock_id_2].unlock();
			}
			F[0] = F[1] = -3;
			sto[0] = 0;
			sto[1] = 0;
			PC = label_ad_2;
		}
		if (F[1] == 1)
			PC = label_ad;
	}
}

void init()
{
	hs_p = _memory.size();
	_memory.resize(hs_p + 4 * 1024 * 1024);
	_re[29].lock();
	_re[29] = _memory.size();
	if (_label.find("main") != _label.end())
		PC = _label["main"];
	else
		PC = -1;
	for (int i = 0; i < 5; ++i)
	{
		is_run[i] = 1;
		F[i] = -3;
	}
}

int main(int args, char *argv[])
{
	std::ios::sync_with_stdio(false);
	_file.open(argv[1]);
	load_p();
	_file.close();
	init();
	run();
	return 0;
}
